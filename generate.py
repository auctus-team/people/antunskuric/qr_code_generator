import qrcode

# Link for website
input_data = "https://gitlab.inria.fr/auctus-team/people/antunskuric/papers/reachable_space"

# color of the qrcode
qr_color ='black'
background_color = 'white'

# name of the output file
file_name = "gitlab_qr.png"

# Creating an instance of qrcode
qr = qrcode.QRCode(
        version=1,
        box_size=10,
        border=5)
qr.add_data(input_data)
qr.make(fit=True)
img = qr.make_image(fill_color=qr_color, back_color=background_color)
img.save(file_name)