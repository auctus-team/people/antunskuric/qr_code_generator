# Simple QR code generator in python

Using the `qrcode` package.


## Install qrcode 

with pip 
```
pip install qrcode
```

or using anaconda
```
conda create -f env.yaml
conda activate qr_code
```

## Config the qr code generation 
In the file `generate.py` update the variables

```python
# Link for website
input_data = "url/to/your/site"

# color of the qrcode
qr_color ='black'
background_color = 'white'

# name of the output file
file_name = "gitlab_qr.png"
```

## Generate the qr

In your shell run
```
python generate.py
```

![](gitlab_qr.png)
